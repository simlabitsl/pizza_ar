﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideMenuScript : MonoBehaviour {

	public static SideMenuScript Instance;
	// Scroll Views
	[Header("Scroll Views")]
	public GameObject CrustScrollView;
	public GameObject ToppingsScrollView;
	public GameObject SizeScrollView;
	// Buttons
	[Header("Buttons")]
	public GameObject CrustButton;
	public GameObject ToppingsButton;
	public GameObject PlaceOrderButton;
	public GameObject BackButton;
	// Buttons
	[Header("Panels")]
	public GameObject OrderPanel;
	public GameObject PricePanel;
	public GameObject FinalPanel;
	[Header("Texts")]
	public GameObject ItemPrefab;
	public GameObject ServiceCharge;
	public GameObject TotalPrice;
	public GameObject PositionSample;

	//Private
	private List<GameObject> itemTexts = new List<GameObject>();
	// Use this for initialization
	void Awake ()
	{
		if (!Instance) {
			Instance = this;
		}
	}

	public void InitiateItemTexts(string strVal, float fltVal, float yVal)
	{
		GameObject textItem =(GameObject) Instantiate(ItemPrefab);
		textItem.transform.SetParent( OrderPanel.transform);
		textItem.transform.localScale = new Vector3(1, 1, 1);
		RectTransform trans = textItem.GetComponent<RectTransform> ();
		RectTransform sampleTrans = PositionSample.GetComponent<RectTransform> ();
		trans.localPosition = new Vector3 (sampleTrans.localPosition.x, (sampleTrans.localPosition.y - (yVal * (trans.rect.height/2))), sampleTrans.localPosition.z);
		textItem.GetComponent<Text> ().text = strVal + " : Rs." + fltVal.ToString("0.00");
		itemTexts.Add (textItem);
	}

	public void ClearGarbage()
	{
		foreach (var item in itemTexts) {
			Destroy (item);
		}
	}

	public void SetMyOrdersValues()
	{
		ServiceCharge.GetComponent<Text>().text = "Service Charge ("+ PriceManager.Instance.serviceCharge +"%) : Rs. " + PriceManager.Instance.GetServiceChargePrice().ToString("0.00");;
		TotalPrice.GetComponent<Text>().text = "Total : Rs. " + PriceManager.Instance.GetTotalPrice().ToString("0.00");;
	}

	public void ToggleCrustScrollView()
	{
		ToppingsScrollView.SetActive (false);
		if (CrustScrollView.activeSelf) {
			CrustScrollView.SetActive (false);
			SizeScrollView.SetActive (false);
		} else {
			CrustScrollView.SetActive (true);
			SizeScrollView.SetActive (false);
		}
	}

	public void ToggleToppingsScrollView()
	{
		CrustScrollView.SetActive (false);
		SizeScrollView.SetActive (false);
		if (ToppingsScrollView.activeSelf) {
			ToppingsScrollView.SetActive (false);
		} else {
			ToppingsScrollView.SetActive (true);
		}
	}

	public void ShowSizeScrollView()
	{
		SizeScrollView.SetActive (true);
	}

	public void HideSizeScrollView()
	{
		SizeScrollView.SetActive (false);
	}

	public void ShowCrustScrollView()
	{
		CrustScrollView.SetActive (true);
	}

	public void HideCrustScrollView()
	{
		CrustScrollView.SetActive (false);
	}

	public void ShowPricePanel()
	{
		PricePanel.SetActive (true);
	}

	public void HidePricePanel()
	{
		PricePanel.SetActive (false);
	}

	public void ShowFinalPanel()
	{
		FinalPanel.SetActive (true);
	}

	public void HideFinalPanel()
	{
		FinalPanel.SetActive (false);
	}

	public void HideToppingsScrollView()
	{
		ToppingsScrollView.SetActive (false);
	}

	public void EnableToppingsButton()
	{
		ToppingsButton.GetComponent<Button> ().interactable = true;
	}

	public void DisableToppingsButton()
	{
		ToppingsButton.GetComponent<Button> ().interactable = false;
	}

	public void EnableCrustButton()
	{
		CrustButton.GetComponent<Button> ().interactable = true;
	}

	public void DisableCrustButton()
	{
		CrustButton.GetComponent<Button> ().interactable = false;
	}

	public void EnablePlaceOrderButton()
	{
		PlaceOrderButton.GetComponent<Button> ().interactable = true;
	}

	public void DisablePlaceOrderButton()
	{
		PlaceOrderButton.GetComponent<Button> ().interactable = false;
	}

	public void HidePlaceOrderButton()
	{
		PlaceOrderButton.SetActive (false);
	}

	public void ShowPlaceOrderButton()
	{
		PlaceOrderButton.SetActive (true);
	}

	public void ShowOrderPanel()
	{
		OrderPanel.SetActive (true);
	}

	public void HideOrderPanel()
	{
		OrderPanel.SetActive (false);
	}

	public void HideBackButton()
	{
		BackButton.SetActive (false);
	}

	public void ShowBackButton()
	{
		BackButton.SetActive (true);
	}

	// Use this for initialization
	void Start () {
		ToppingsScrollView.SetActive (false);
		CrustScrollView.SetActive (false);
		SizeScrollView.SetActive (false);
		DisableToppingsButton ();
		DisablePlaceOrderButton ();
		EnableCrustButton ();
		OrderPanel.SetActive (false);
		BackButton.SetActive (false);
		HideFinalPanel ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
