﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

	public GameObject IntroVideo;

	public void GoToMainScene()
	{
//		SceneManager.LoadScene("Main");
		IntroVideo.SetActive(true);
	}

	// Use this for initialization
	void Start () {
		IntroVideo.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
