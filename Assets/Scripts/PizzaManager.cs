﻿using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine;

public class PizzaManager : MonoBehaviour
{
	// Public
	public int maxToppingCount = 2;
	public float toppingEnableTime = 2f;
	public VuforiaBehaviour vuforiaCameraBehaviour;
	public static PizzaManager Instance;
	// Particle
	[Header("Particle")]
	public GameObject OliveParticle;
	public GameObject PepperoniParticle;
	public GameObject MushroomParticle;
	public GameObject BellPepersParticle;

	[Header("Toppings")]
	public GameObject OliveTopping;
	public GameObject PepperoniTopping;
	public GameObject MushroomTopping;
	public GameObject BellPepersTopping;

	[Header("Video")]
	public GameObject MakingVideo;

	[Header("Colliders")]
	public GameObject PizzaCollider;

	// privates
	public enum Crust 
	{
		None = 0,
		ThinCrust = 1,
		Pan = 2,
		Stuffed = 3
	}

	public enum Size 
	{
		None = 0,
		Large = 1,
		Medium = 2,
		Small = 3
	}

	public enum Topping 
	{
		None = 0,
		Olive = 1,
		Pepperoni = 2,
		Mushroom = 3,
		BellPepers = 4
	}
			
	bool isCrustSelected = false;
	Crust selectedCrust = Crust.None;
	bool isSizeSelected = false;
	Size selectedSize = Size.None;
	Topping selectedTopping = Topping.None;
	Dictionary<string, int> toppingCountDic = new Dictionary<string, int>();
	

	void Awake ()
	{
		if (!Instance) {
			Instance = this;
		}
	}

	void Start()
	{
		OliveTopping.SetActive (false);
		PepperoniTopping.SetActive (false);
		MushroomTopping.SetActive (false);
		BellPepersTopping.SetActive (false);
		MakingVideo.SetActive (false);
		PizzaCollider.GetComponent<MeshRenderer> ().enabled = false;
	}

	public void ClickBackButton()
	{
		resetAll ();
		MakingVideo.SetActive (false);
		vuforiaCameraBehaviour.enabled = false;
		SideMenuScript.Instance.DisableCrustButton ();
		SideMenuScript.Instance.HidePlaceOrderButton ();
		SideMenuScript.Instance.HidePricePanel ();
		SideMenuScript.Instance.ShowFinalPanel ();
	}

	public void ClickViewMaking()
	{
		SideMenuScript.Instance.HideFinalPanel ();
		vuforiaCameraBehaviour.enabled = true;
		MakingVideo.SetActive (true);
		SideMenuScript.Instance.ShowBackButton ();
//		MakingVideo.GetComponent<VideoBackgroundBehaviour>().
	}

	public void ClickConfirmOrder()
	{
		resetAll ();
		vuforiaCameraBehaviour.enabled = false;
		SideMenuScript.Instance.DisableCrustButton ();
		SideMenuScript.Instance.HidePlaceOrderButton ();
		SideMenuScript.Instance.HidePricePanel ();
		SideMenuScript.Instance.ShowFinalPanel ();
	}

	public void ClickCancelOrder()
	{
		resetAll ();
	}

	public void ClickNewOrder()
	{
		resetAll ();
	}

	public void ClickFacebook()
	{
		Application.OpenURL("https://www.facebook.com/WatersEdgeSL/");
	}


	public void ClickPlaceOrder()
	{
		vuforiaCameraBehaviour.enabled = false;
		SideMenuScript.Instance.HidePlaceOrderButton ();
		SideMenuScript.Instance.ShowOrderPanel ();
		SideMenuScript.Instance.DisableCrustButton ();
		SideMenuScript.Instance.DisableToppingsButton ();
		SideMenuScript.Instance.HideToppingsScrollView ();
		SideMenuScript.Instance.HideCrustScrollView ();
		SideMenuScript.Instance.HideSizeScrollView ();
		SideMenuScript.Instance.HidePricePanel ();

		int i = 0;
		foreach (KeyValuePair<string, float> itemprice in PriceManager.Instance.Price) {
			SideMenuScript.Instance.InitiateItemTexts (itemprice.Key, itemprice.Value, i++);
		}

		SideMenuScript.Instance.SetMyOrdersValues ();
	}

	public void ClickToppingSelect (int selected)
	{
		selectedTopping = (Topping)selected;
		if (toppingCountDic.ContainsKey (selectedTopping.ToString ())) {
			toppingCountDic [selectedTopping.ToString ()]++;
		} else {
			toppingCountDic.Add (selectedTopping.ToString (), 1);
		}
		ProcessToppings ();
	}
		
	public void ClickCrustSelect (int selected)
	{
		resetAll ();
		isCrustSelected = true;
		selectedCrust = (Crust)selected;
		SideMenuScript.Instance.HideCrustScrollView ();
		SideMenuScript.Instance.ShowSizeScrollView ();
		Debug.Log (selectedCrust.ToString());
	}

	public void ClickSizeSelect (int selected)
	{
		isSizeSelected = true;
		selectedSize = (Size)selected;
		CalculateCrustSizePrice ();
		SideMenuScript.Instance.HideSizeScrollView ();
		SideMenuScript.Instance.EnableToppingsButton ();
		SideMenuScript.Instance.EnablePlaceOrderButton ();
	}

	public void ProcessToppings()
	{
		switch (selectedTopping) {
		case Topping.Olive:
			if (toppingCountDic [selectedTopping.ToString ()] <= maxToppingCount) {	
				OliveParticle.GetComponent<ParticleSystem> ().Play ();
				PriceManager.Instance.AddPrice (selectedTopping.ToString (), 40f);
				StartCoroutine (EnableToppingLayer (OliveTopping));
			} else {
				// To Do: Need to add alert that Topping adding has exceeded.
			}
			break;
		case Topping.Pepperoni:
			if (toppingCountDic [selectedTopping.ToString ()] <= maxToppingCount) {	
				PepperoniParticle.GetComponent<ParticleSystem> ().Play ();
				PriceManager.Instance.AddPrice (selectedTopping.ToString (), 60f);
				StartCoroutine (EnableToppingLayer (PepperoniTopping));
			} else {
				// To Do: Need to add alert that Topping adding has exceeded.
			}
			break;
		case Topping.Mushroom:
			if (toppingCountDic [selectedTopping.ToString ()] <= maxToppingCount) {	
				MushroomParticle.GetComponent<ParticleSystem> ().Play ();
				PriceManager.Instance.AddPrice (selectedTopping.ToString (), 60f);
				StartCoroutine (EnableToppingLayer (MushroomTopping));
			} else {
				// To Do: Need to add alert that Topping adding has exceeded.
			}
			break;
		case Topping.BellPepers:
			if (toppingCountDic [selectedTopping.ToString ()] <= maxToppingCount) {	
				BellPepersParticle.GetComponent<ParticleSystem> ().Play ();
				PriceManager.Instance.AddPrice (selectedTopping.ToString (), 60f);
				StartCoroutine (EnableToppingLayer (BellPepersTopping));
			} else {
				// To Do: Need to add alert that Topping adding has exceeded.
			}
			break;
		default:
			break;
		}
	}

	IEnumerator EnableToppingLayer(GameObject layer)
	{
		yield return new WaitForSeconds(toppingEnableTime);
		layer.SetActive (true);
	}

	public void CalculateCrustSizePrice()
	{
		PriceManager.Instance.RemovePrice ();
		float price = 0f;
		switch (selectedCrust) {
		case Crust.ThinCrust:
			switch (selectedSize) {
			case Size.Large:
				price = 1200f;
				break;
			case Size.Medium:
				price = 900f;
				break;
			case Size.Small:
				price = 550f;
				break;
			default:
				break;
			}
			break;
		case Crust.Pan:
			switch (selectedSize) {
			case Size.Large:
				price = 1000;
				break;
			case Size.Medium:
				price = 700;
				break;
			case Size.Small:
				price = 450;
				break;
			default:
				break;
			}
			break;
		case Crust.Stuffed:
			switch (selectedSize) {
			case Size.Large:
				price = 1350;
				break;
			case Size.Medium:
				price = 1100;
				break;
			case Size.Small:
				price = 650;
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}

		PriceManager.Instance.AddPrice (selectedCrust.ToString() + " " + selectedSize.ToString() ,price);
	}

	private void resetAll()
	{
		PriceManager.Instance.RemovePrice ();
		SideMenuScript.Instance.EnableCrustButton ();
		toppingCountDic.Clear ();
		isCrustSelected = false;
		selectedCrust = Crust.None;
		isSizeSelected = false;
		selectedSize = Size.None;
		SideMenuScript.Instance.DisableToppingsButton ();
		SideMenuScript.Instance.DisablePlaceOrderButton ();
		SideMenuScript.Instance.HideOrderPanel ();
		SideMenuScript.Instance.ShowPlaceOrderButton ();
		SideMenuScript.Instance.ClearGarbage ();
		SideMenuScript.Instance.ShowPricePanel ();
		SideMenuScript.Instance.HideFinalPanel ();
		SideMenuScript.Instance.HideBackButton ();
		OliveTopping.SetActive (false);
		PepperoniTopping.SetActive (false);
		MushroomTopping.SetActive (false);
		BellPepersTopping.SetActive (false);
		vuforiaCameraBehaviour.enabled = true;
		Debug.Log ("resetAll");
	}
}
