﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PriceManager : MonoBehaviour
{

	public Text priceText;
	public float serviceCharge = 5f;
	public Dictionary<string,float> Price = new Dictionary<string, float> ();
	float tot = 0;
	public static PriceManager Instance;

	// Use this for initialization
	void Awake ()
	{
		if (!Instance) {
			Instance = this;
		}
	}

	public void AddPrice (string itemName, float itemPrice)
	{		
		Price.Add (itemName,itemPrice);
		ShowPrice ();
	}

	public void RemovePrice ()
	{		
		Price.Clear ();
		ShowPrice ();
	}

	public float GetServiceChargePrice()
	{
		return (tot / 100f) * serviceCharge; 
	}

	public float GetTotalPrice()
	{
		return tot + GetServiceChargePrice ();
	}

	public void ShowPrice ()
	{
		tot = 0;
		foreach (KeyValuePair<string, float> itemprice in Price)
		{
			tot = tot + itemprice.Value;
		}
		priceText.text = "Total Price(LKR): " + tot.ToString () + ".00";
	}
}
